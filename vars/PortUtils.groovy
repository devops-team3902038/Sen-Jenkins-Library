
    def selectRandomAvailablePort(int minPort, int maxPort) {
        def numberOfPortsToCheck = maxPort - minPort + 1
        def portsToCheck = (minPort..maxPort).toList()
        Collections.shuffle(portsToCheck)

        for (int i = 0; i < numberOfPortsToCheck; i++) {
            def portToCheck = portsToCheck[i]
            if (isPortAvailable(portToCheck) && !isPortInUseForDocker(portToCheck)) {
                return portToCheck
            }
        }

        return null
    }

    def isPortAvailable(int port) {
        def socket
        try {
            socket = new Socket("localhost", port)
            return false // Port is already in use
        } catch (Exception e) {
            return true // Port is available
        } finally {
            if (socket) {
                socket.close()
            }
        }
    }

    def isPortInUseForDocker(int port) {
        def dockerPsOutput = sh(script: "docker ps --format '{{.Ports}}'", returnStdout: true).trim()

        // Check if the Docker container port mapping contains ":$port->80/tcp"
        return dockerPsOutput.contains(":$port->80/tcp")
    }

    def listPortsInUseForDocker(int minPort, int maxPort) {
        def usedPorts = []
        for (int port = minPort; port <= maxPort; port++) {
            if (isPortInUseForDocker(port)) {
                usedPorts.add(port)
            }
        }
        return usedPorts
    }

